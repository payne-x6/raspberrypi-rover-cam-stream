# Program name
program=CamStream

# Object files
OBJ=CamStream.o

# Include headers
IH=-I/usr/local/include

# Libraries
LDEP=-lraspicam

# Optimalization
OPT=-O2

.PHONY: build
.PHONY: install
.PHONY: uninstall
.PHONY: clean
.PHONY: distrib

build: ${program}

install: build
	cp ${program} /usr/local/bin

uninstall:
	rm -f /usr/local/bin/${program}

clean:
	rm -f *.o ${program}

distrib:
	tar -c CamStream.cpp Makefile > c17.tar; \
	gzip c17.tar

${program}: ${OBJ}
	g++ ${OBJ} -o ${program} ${OPT} ${IH} ${LDEP}

CamStream.o: src/CamStream.cpp
	g++ src/CamStream.cpp -c ${OPT}  
