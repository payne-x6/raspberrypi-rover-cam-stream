/*
 * RaspiCamStream.h
 *
 *  Created on: Oct 27, 2015
 *  Author: Jan Hak
 */

#ifndef SRC_RASPICAMSTREAM_H_
#define SRC_RASPICAMSTREAM_H_

#include <raspicam/raspicam.h>
#include <raspicam/raspicamtypes.h>

class RaspiCamStream {
private:
	raspicam::RaspiCam camera;
public:
	RaspiCamStream();
	~RaspiCamStream();
	raspicam::RaspiCam &getCameraInstance();
	void stream();

};

#endif /* SRC_RASPICAMSTREAM_H_ */
