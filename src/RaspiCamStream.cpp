/*
 * RaspiCamStream.cpp
 *
 *  Created on: Oct 27, 2015
 *      Author: pi
 */

#include "RaspiCamStream.h"

RaspiCamStream::RaspiCamStream() {
	camera.setWidth(800);
	camera.setHeight(600);
	camera.setBrightness(50);

	camera.setSharpness(0);
	camera.setContrast(0);
	camera.setSaturation(0);
	camera.setShutterSpeed(0);
	camera.setISO(400);
	camera.setVideoStabilization(true);
	camera.setExposureCompensation(0);

	//Camera.setFormat(raspicam::RASPICAM_FORMAT_RGB);
	camera.setFormat(raspicam::RASPICAM_FORMAT_GRAY);
	//Camera.setFormat(raspicam::RASPICAM_FORMAT_YUV420);
	camera.setExposure (raspicam::RASPICAM_EXPOSURE_AUTO);
	camera.setAWB(raspicam::RASPICAM_AWB_AUTO);
	    //Camera.setAWB_RB(raspicam::RASPICAM_AWB_AUTO, raspicam::RASPICAM_AWB_AUTO);
}

RaspiCamStream::~RaspiCamStream() {
	camera.release();
}

raspicam::RaspiCam &RaspiCamStream::getCameraInstance() {
	return camera;
}

void RaspiCamStream::stream() {
	int buffSize = camera.getImageBufferSize();
	unsigned char *data = new unsigned char[buffSize];
	camera.grab();
	camera.retrieve(data);
	//sendto(clientSock, data, buffSize, 0, "10.10.115.37", default_destination_size);

}
